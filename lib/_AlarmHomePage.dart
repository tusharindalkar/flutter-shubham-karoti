import 'dart:isolate';
import 'dart:math';
import 'dart:ui';

import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shubham_karoti_kalynam/main.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'constants/StringResources.dart';


class AlarmHomePage extends StatefulWidget {
  AlarmHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AlarmHomePageState createState() => _AlarmHomePageState();
}

class _AlarmHomePageState extends State<AlarmHomePage> {
  int _counter = 0;
   static String countKey = 'count';
   static String isolateName = 'isolate';
  final ReceivePort port = ReceivePort();
  SharedPreferences prefs;
DateTime dateTime;
Duration duration;
  @override
  void initState() {
    super.initState();
    initAlarm();
    AndroidAlarmManager.initialize();


    // Register for events from the background isolate. These messages will
    // always coincide with an alarm firing.
    port.listen((_) async => await _incrementCounter());
  }

  Future<void> _incrementCounter() async {
    print('Increment counter!');

    // Ensure we've loaded the updated count from the background isolate.
    await prefs.reload();

    setState(() {
      _counter++;
    });
  }

  // The background
  static SendPort uiSendPort;

  // The callback for our alarm
  static Future<void> callback() async {
    print('Alarm fired!');
    Fluttertoast.showToast(
        msg: StringResources.DEFAULT_SIZE,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.orange,
        textColor: Colors.white,
        fontSize: 16.0);
    // Get the previous cached count and increment it.
    final prefs = await SharedPreferences.getInstance();
    if (prefs!=null) {
      int currentCount = prefs.getInt(countKey);
      await prefs.setInt(countKey, currentCount + 1);
    }


    // This will be null if we're running in the background.
    uiSendPort ??= IsolateNameServer.lookupPortByName(isolateName);
    uiSendPort?.send(null);
  }
  @override
  Widget build(BuildContext context) {
    // TODO(jackson): This has been deprecated and should be replaced
    // with `headline4` when it's available on all the versions of
    // Flutter that we test.
    // ignore: deprecated_member_use
    final textStyle = Theme.of(context).textTheme.display1;
    return Scaffold(
      appBar: AppBar(
        title: Text("widget.title"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Alarm fired $_counter times',
              style: textStyle,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Total alarms fired: ',
                  style: textStyle,
                ),
                prefs!=null?
                Text(
                  prefs.getInt(countKey).toString(),
                  key: ValueKey('BackgroundCountText'),
                  style: textStyle,
                ):Text("NO Alarms"),
              ],
            ),
            RaisedButton(

              child: Text(
                'Schedule OneShot Alarm',
              ),
              key: ValueKey('RegisterOneShotAlarm'),
              onPressed: () async {
                DatePicker.showTime12hPicker(context, showTitleActions: true, onChanged: (date) {
                  print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
                }, onConfirm: (date) {
                  dateTime=date;
                  print('confirm $date');
                }, currentTime: DateTime.now());

                await AndroidAlarmManager.oneShotAt(
                 dateTime,
                  // Ensure we have a unique alarm ID.
                  Random().nextInt(pow(2, 31)),
                  callback,
                  alarmClock: true,
                  allowWhileIdle: true,
                  rescheduleOnReboot: true,
                  exact: true,
                  wakeup: true,
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Future <void> initAlarm() async{
    // Register the UI isolate's SendPort to allow for communication from the
    // background isolate.
    IsolateNameServer.registerPortWithName(
      port.sendPort,
      isolateName,
    );
    prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(countKey)) {
      await prefs.setInt(countKey, 0);
    }
  }
}