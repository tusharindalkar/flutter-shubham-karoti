import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'package:shubham_karoti_kalynam/constants/StringResources.dart';
import 'package:shubham_karoti_kalynam/networking/ApiKeys.dart';
import 'package:shubham_karoti_kalynam/networking/ApiProvider.dart';
import 'package:shubham_karoti_kalynam/networking/EndApi.dart';
import 'package:shubham_karoti_kalynam/view/MyHomePage.dart';

class ApiHandler
{
  static List<dynamic> decodeResponse(String responseBody) {
    Map<String,dynamic>map=json.decode(responseBody);
    Map mapResult=map[ApiKeys.RESULT];
    List<dynamic>list;
    list=mapResult[ApiKeys.LIST];
    return list;
  }


   static Future<List<dynamic>> requestApi(String endApi) async
   {
    var client=new http.Client();
    try
    {
      final response = await client.get(ApiProvider.baseUrl+endApi);
      if (response.statusCode == 200)
      {
        return decodeResponse(response.body);
      } else {
        throw Exception(StringResources.APIERROR);
      }
      }
    finally
    {
      client.close();
    }
  }
  }
