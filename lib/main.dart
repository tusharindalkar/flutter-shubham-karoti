import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shubham_karoti_kalynam/constants/StringResources.dart';
import 'package:shubham_karoti_kalynam/view/MyHomePage.dart';
import 'package:hive/hive.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
const String playListBoxName="playList";
const String newPlayList="newPlayList";
const String countKey = 'count';

/// The name associated with the UI isolate's [SendPort].
const String isolateName = 'isolate';

/// A port used to communicate from a background isolate to the UI isolate.
final ReceivePort port = ReceivePort();

/// Global [SharedPreferences] object.
SharedPreferences prefs;

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  Directory document=await getApplicationDocumentsDirectory();
  Hive.init(document.path);/*
  Hive.registerAdapter(PlaylistAdapter());
  await Hive.openBox<Playlist>(playListBoxName);
*/
  await Hive.openBox<String>(newPlayList);
  await Hive.openBox<String>("notifications");

  runApp(new MaterialApp(
    home: new MyAppSt(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyAppSt extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyApp(),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String title=StringResources.APP_NAME;
  int id=0;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var android=new AndroidInitializationSettings('mipmap/ic_launcher');
    Timer(
        Duration(seconds: 4),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => MyHomePage(id,title,false))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            margin: EdgeInsets.fromLTRB(0, 200, 0, 0),
            child: Center(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/images/welcomescr.png',
                    height: 240, width: 330),
                new Text("सुस्वागतम",
                    style: GoogleFonts.pacifico(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.black87)),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: new Text(StringResources.APP_NAME,
                      style: GoogleFonts.pacifico(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.black87)),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.orange,
                    valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                    strokeWidth: 5,
                  ),
                )
              ],
            ))));
  }
}
