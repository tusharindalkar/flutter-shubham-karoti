import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'dart:math';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shubham_karoti_kalynam/PlayerModel.dart';
import 'package:shubham_karoti_kalynam/constants/StringResources.dart';
import 'package:shubham_karoti_kalynam/networking/ApiKeys.dart';
import 'package:shubham_karoti_kalynam/view/MyHomePage.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:shubham_karoti_kalynam/main.dart';
import 'package:shubham_karoti_kalynam/colors/color.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:expandable/expandable.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';

class PlayListWidget extends StatefulWidget
{
  _PlayListWidgetState createState()=> _PlayListWidgetState();
}
enum PlayerState { stopped, playing, paused }

class _PlayListWidgetState extends State<PlayListWidget>
{

  List urlList = [];
 // List playingList = [];
  static AudioPlayer _audioPlayer;
  static AudioPlayer _audioPlayerAlarm;

  AudioPlayerState _audioPlayerState;
  Duration _duration;
  Duration _position;
  int indexQueue = 0;
  int ind=0;
  String url;
  static String urlAlarm;
  double descriptionSize = 16;
  double titleSize = 16;
   List<dynamic> items=[];


  PlayerState _playerState = PlayerState.stopped;
  List<Color> _colors = [Colors.green, Colors.redAccent];
  List<double> _stops = [0.0, 0.7];
  StreamSubscription _durationSubsScription;
  StreamSubscription _positionSubsScription;
  StreamSubscription _playerCompleteSubsScription;
  StreamSubscription _playerErrorSubsScription;
  StreamSubscription _playerStateSubsScription;
  StreamSubscription _seekCompleteSubscription;
  ScrollController scrollController = new ScrollController();

  get _isPlaying => _playerState == PlayerState.playing;

  get _isPaused => _playerState == PlayerState.paused;

  get _durationText => _duration?.toString()?.split('.')?.first ?? '';

  get _positionText => _position?.toString()?.split('.')?.first ?? '';
  final _ScaffoldKey = new GlobalKey<ScaffoldState>();
  PlayerModel playerModel;
  PanelController _panelControler = new PanelController();

  List playingList = [];
  List<dynamic>fList=[];
  List<dynamic>slist=[];
  Box<String>boxnewPlayList;
  var details=new Map();
  String str;
  String idStr;
  var idMap = new Map();
  var idTempMap=new Map();
  bool isPanelOpen=false;
  int _counter = 0;
  static String countKey = 'count';
  static String isolateName = 'isolate';
  final ReceivePort port = ReceivePort();
  SharedPreferences prefs;
  DateTime dateTime;
  static var playPosition;
  @override
  void initState(){
    // TODO: implement initState
    super.initState();
    _initAudioPlayer();
    indexQueue = 0;
    boxnewPlayList=Hive.box(newPlayList);

    str=boxnewPlayList.get("playList");
    idStr=boxnewPlayList.get("idMap");
    if (idStr != null) {
      idStr = boxnewPlayList.get("idMap");
      idMap = json.decode(idStr);
    }
    if (items.isNotEmpty || items!=null) {
      if (str!=null) {
        items=json.decode(str);
        for (int i = 0; i < items.length; i++) {
          playingList.add(PlayerModel<String>("item $i"));
          urlList.add(items[i]['link']);
        }
      }
    }
    hidePanel();
    initAlarm();
    AndroidAlarmManager.initialize();


    // Register for events from the background isolate. These messages will
    // always coincide with an alarm firing.
    port.listen((_) async => await _incrementCounter());
  }
  static SendPort uiSendPort;

  var Position;
  @override
  void dispose() {
    _audioPlayer.dispose();
    _durationSubsScription?.cancel();
    _positionSubsScription?.cancel();
    _playerCompleteSubsScription?.cancel();
    _playerErrorSubsScription?.cancel();
    _playerStateSubsScription?.cancel();
    _seekCompleteSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (items.isEmpty) {
      return Scaffold(
        appBar:AppBar(title: Text(StringResources.PLAY_LIST),backgroundColor:Colors.orange,),
        body:Container(
          child:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child:Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment:CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(StringResources.EMPTY_PLAYlIST,
                        style:TextStyle(fontSize:17,color:Colors.orange,fontWeight:FontWeight.bold),),
                      Padding(padding: EdgeInsets.fromLTRB(0, 0, 40, 0),
                        child:IconButton(
                          icon: Icon(Icons.hourglass_empty,size: 45,),
                        ),
                      ),
                    ]
                ),
              ),
             Row(
               mainAxisAlignment: MainAxisAlignment.end,
               mainAxisSize: MainAxisSize.max,
               children: <Widget>[
                  Container(
                    margin:EdgeInsets.fromLTRB(0, 80, 18, 10),
                      child:                 FloatingActionButton(child:
                      IconButton(icon:Icon(Icons.queue_music,color:Colors.white,size: 28,)
                        ,),backgroundColor:Colors.orange,onPressed:(){
                        Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => MyHomePage(0,StringResources.APP_NAME,false)));
                      },)
                  ),
               ],
             )
            ],
          )
        ),
      );
    }
    if (items.isNotEmpty) {
      return WillPopScope(
        onWillPop: ()=>_backPressed(),
        child:  new Scaffold(
          appBar: AppBar(
            title:Text(StringResources.PLAY_LIST),
            backgroundColor:color.colorConvert(color.APP_COLOR),
            actions: <Widget>[
              Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(icon:Icon(Icons.zoom_in,color: Colors.white,size: 38,),onPressed:(){
                      zoomIN();
                    })

                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(10, 0, 12, 0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(icon:Icon(Icons.zoom_out,color: Colors.white,size: 38,),onPressed:()=>zoomOut())

                  ],
                ),
              )
            ],
          ),
          body:
          SlidingUpPanel(
            minHeight: 162,
            maxHeight: 160,
            controller: _panelControler,
            header: Container(
              height: 152,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: items[indexQueue]['title']!="NULL"?Container(
                              margin: EdgeInsets.fromLTRB(8, 14, 0, 0),
                              child: Text(
                                items[indexQueue]['title'],
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.black45),
                                overflow:TextOverflow.ellipsis,
                                softWrap: true,
                              ),
                            ):Text("\n"),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(10, 8,25, 0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: _isPlaying
                                  ? Text(
                                _position != null
                                    ? '${_positionText ?? ''}'
                                    : _duration != null
                                    ? _durationText
                                    : '',
                                style: TextStyle(fontSize: 12.0),
                              )
                                  : Text("0:0:00"),
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 5, 10, 0),
                                height: 10,
                                child:SliderTheme(
                                  data: SliderTheme.of(context).copyWith(
                                    activeTrackColor: Colors.red[700],
                                    inactiveTrackColor: Colors.red[100],
                                    trackShape: RectangularSliderTrackShape(),
                                    trackHeight: 4.0,
                                    thumbColor: Colors.redAccent,
                                    thumbShape: RoundSliderThumbShape(enabledThumbRadius: 8.0),
                                    overlayColor: Colors.red.withAlpha(32),
                                    overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
                                  ),

                                child: Slider(
                                  activeColor:color.colorConvert(color.APP_COLOR),
                                  onChanged: (v) {
                                    Position = v * _duration.inMilliseconds;
                                    _audioPlayer.seek(
                                        Duration(milliseconds: Position.round()));
                                  },
                                  value: (_position != null &&
                                      _duration != null &&
                                      _position.inMilliseconds > 0 &&
                                      _position.inMilliseconds <
                                          _duration.inMilliseconds)
                                      ? _position.inMilliseconds /
                                      _duration.inMilliseconds
                                      : 0.0,
                                ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: _isPlaying
                                  ? Text(
                                _position != null
                                    ? '${_durationText ?? ''}'
                                    : _duration != null
                                    ? _durationText
                                    : '',
                                style: TextStyle(fontSize: 12.0),
                              )
                                  : Text("0:0:00"),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
                        child:                     Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 19),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.skip_previous,
                                    size: 50,
                                  ),
                                  color: Colors.black45,
                                  onPressed: () {
                                    skipPrevious();
                                  },
                                ),
                              ),
                            ),
                            Expanded(
                            //  child:
                                child: Container(
                                  child: _isPlaying
                                      ? IconButton(
                                    key: Key('pause_button'),
                                    onPressed: () {
                                      playingList[indexQueue].isSelected = false;
                                      _pause();
                                    },
                                    iconSize: 45.0,
                                    icon: Icon(Icons.pause),
                                    color: Colors.black45,
                                  )
                                      : IconButton(
                                    iconSize: 45.0,
                                    icon: playingList[indexQueue].isSelected
                                        ? Icon(Icons.pause)
                                        : Icon(Icons.play_arrow),
                                    color:color.colorConvert(color.APP_COLOR),
                                    tooltip: "play_pause",
                                    key: Key('play_button'),
                                    onPressed: () {
                                      playingList[indexQueue].isSelected = true;
                                      print(url);
                                      setState(() => PlayerState.paused);
                                      setState(() {
                                        _playerState = PlayerState.paused;
                                        _duration = null;
                                        _position = null;
                                        _play();
                                      });
                                    },
                                  ),
                                )),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.skip_next,
                                    color: Colors.black45,
                                    size: 50,
                                  ),
                                  onPressed: () {
                                    skipNext();
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //  ),
            panel: Center(
            ),
            body:
            Container(
              color: color.colorConvert("#ECEFF1"),

                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
             // width: MediaQuery.of(context).size.width-40,
              child:
              Column(
                children: <Widget>[
                  new Expanded(
                    child:
                      ReorderableListView(children: <Widget>[
                        for(int item=0;item<items.length;item++)
                Container(
                    key: ValueKey(item),
                    child:Padding(padding:EdgeInsets.fromLTRB(4, 0, 4, 0),
              child:Card(
                elevation:0,
                color: color.colorConvert("#ffffff"),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                child:  ExpansionTile(
                  trailing: Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child:
                     Row(
                       mainAxisSize: MainAxisSize.min,
                       crossAxisAlignment:CrossAxisAlignment.center,
                       mainAxisAlignment:MainAxisAlignment.spaceBetween,
                       children: <Widget>[
                         IconButton(icon:Icon(Icons.alarm_add,color:Colors.black87,size: 30,), onPressed:()=>showTimePickerDialog()),
                         items[item]['link']=="NULL"?
                         Text(""): playingList[item].isSelected
                             ?
                         IconButton(
                           onPressed: () {
                             _pause();
                             setState(() {
                               playingList[item]
                                   .isSelected =
                               false;
                             });
                           },
                           iconSize: 38.0,
                           icon: Icon(Icons.pause),
                           color: Colors.black45,
                         )
                             : IconButton(
                           onPressed: () {
                             setState(() {
                               ind=indexQueue;
                               print(ind);
                               playingList[ind].isSelected=false;
                               indexQueue = item;
                               url = items[
                               indexQueue]
                               ['link'];
                               print(url);
                               playingList[item]
                                   .isSelected =
                               true;
                             });

                             _play();
                           },
                           iconSize: 38.0,
                           icon: Icon(
                               Icons.play_arrow),
                           color:color.colorConvert(color.APP_COLOR),
                         ),
                         Padding(padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                         child:IconButton(icon: Icon(Icons.delete_forever,size: 25,), onPressed:(){
                           // debugPrint(items[item]["id"]);
                           //  debugPrint(items[item]["id"].toString());
                           print(idMap[items[item]["id"].toString()]);
                           var itemId=items[item]["id"].toString();
                           //var mapId=
                           print(idMap);
                           idMap.remove(items[item]["id"].toString());
                           print(idMap);
                           idStr=json.encode(idMap);
                           boxnewPlayList.put("idMap", idStr);

                           items.removeAt(item);
                           setState(() {
                             str=json.encode(items);
                             boxnewPlayList.put("playList", str);
                           });
                         }),
                         ),
                       ],
                     )
                  ),
                  title:
                  items[item]['title']!="NULL"?
                  Text(items[item]['title'],  overflow:TextOverflow.clip,
                      softWrap: true,
                      style: GoogleFonts.roboto(
                          fontWeight:
                          FontWeight.bold,
                          color:
                          Colors.black54,fontSize: titleSize)):Text(""),
                  children: <Widget>[
                    Container(
                      margin:EdgeInsets.fromLTRB(14, 0, 14, 12),
                      child:
                      items[item]['description']!="NULL"?
                      Text(items[item]['description'], style: GoogleFonts.roboto(
                          fontWeight:
                          FontWeight.bold,
                          fontSize: descriptionSize,
                          color: Colors.black45),):
                          Text(""),

                    )

                  ],
                  initiallyExpanded:false,
                ),
              )
          )
        ),

                      ], onReorder: (oldIndex,newIndex){
                        setState(() {
                          if(newIndex>oldIndex)
                            {
                              newIndex-=1;
                            }
                          final item=items.removeAt(oldIndex);
                          items.insert(newIndex,item);

                          str=json.encode(items);
                          boxnewPlayList.put("playList", str);

                        });
                      })



                  ),
                ],
              ),
            ),
          ),
        ),
      );
  }
    else{
      return Scaffold(
        appBar: AppBar(title:Text(StringResources.PLAY_LIST),
          backgroundColor:Colors.orange,
        ),
        body:
       SafeArea(
      child:Column(
      mainAxisAlignment:MainAxisAlignment.center,
      children: <Widget>[
      Container(
      child:Center(
      child:Text(StringResources.EMPTY_PLAYlIST,
      style:TextStyle(fontSize:17,color:Colors.orange,fontWeight:FontWeight.bold),)
    )
    )
    ],
    )
    ),
      );
    }
  }

  void skipNext() {
    if (indexQueue < items.length) {
      setState(() => _playerState = PlayerState.stopped);
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = null;
        _position = null;
       // playingList[indexQueue].isSelected = false;
        indexQueue < items.length - 1
            ? indexQueue = indexQueue + 1
            : Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("Queue Finished")));
        url = items[indexQueue]['link'];
        //playingList.add(PlayerModel<String>("item $indexQueue"));
        //playingList[indexQueue].isSelected = true;
        _play();
      });
    }
  }

  void zoomIN() async {
    setState(() {
      descriptionSize < 20 ? descriptionSize += 1 : descriptionSize = 20;
      titleSize < 20 ? titleSize += 3 : titleSize = 20;
    });
  }

  void zoomOut() async {
    setState(() {
      descriptionSize >16
          ? descriptionSize -= 1
          :descriptionSize=16;
      titleSize > 18 ? titleSize -= 2 :titleSize=18;
      print(descriptionSize);
    });
  }
  finishZoom(){
    Fluttertoast.showToast(
        msg: StringResources.DEFAULT_SIZE,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.orange,
        textColor: Colors.white,
        fontSize: 16.0
    );
    descriptionSize=15;
    titleSize=18;
  }
  void skipPrevious() {
    if (indexQueue != 0) {
      color:
      Colors.black45;
      setState(() => _playerState = PlayerState.stopped);
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = null;
        _position = null;
      //  playingList[indexQueue].isSelected = false;
        indexQueue = indexQueue - 1;
        url = items[indexQueue]['link'];
       // playingList[indexQueue].isSelected = true;
        _play();
      });
    }
    if (indexQueue == 0) {
      color:
      Colors.black12;
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text("Unavailable")));
    }
  }

  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer();
    _audioPlayerAlarm=AudioPlayer();
    int index = 0;
    _durationSubsScription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => _duration = duration);

      // TODO implemented for iOS, waiting for android impl
      if (Theme.of(context).platform == TargetPlatform.iOS) {
        // (Optional) listen for notification updates in the background
        _audioPlayer.startHeadlessService();
      }
    });

    _positionSubsScription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
          _position = p;
        }));
    _seekCompleteSubscription = _audioPlayer.onSeekComplete.listen((event) {
      //_onSeekComplete();
    });
    _playerCompleteSubsScription =
        _audioPlayer.onPlayerCompletion.listen((event) {
          setState(() {
            _position = _duration;
            _onComplete();
         //   playingList[indexQueue].isSelected = false;
            url = urlList[indexQueue + 1];
            indexQueue = indexQueue + 1;
          //  playingList[indexQueue].isSelected = true;
            _play();
          });
        });

    _playerErrorSubsScription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() => _audioPlayerState = state);
    });
  }

  Future<int> _play() async {
    print("playPos: $playPosition");
    _panelControler.show();
    setState(()=>isPanelOpen=true);
    _duration = null;
    _position = null;
  /*  Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(
        "Please Wait Streaming....",
        style: GoogleFonts.roboto(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize:12),
      ),
      backgroundColor: Colors.white10,
      duration: Duration(seconds: 4),
    ),);*/
     playPosition = (_position != null &&
        _duration != null &&
        _position.inMilliseconds > 0 &&
        _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result = await _audioPlayer.play(url, position: playPosition);
    if (result == 1) setState(() => _playerState = PlayerState.playing);

    _audioPlayer.setPlaybackRate(playbackRate: 1.0);
    return result;
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1) setState(() => _playerState = PlayerState.paused);
    return result;
  }

  void _onComplete() {
    setState(() => _playerState = PlayerState.playing);
  }
  void hidePanel()
  {
    Future.delayed(Duration(microseconds:1),()=>_panelControler.hide());
  }

  _backPressed() {
    setState(() {
      setState(() => _playerState = PlayerState.stopped);
    });
    _audioPlayer.dispose();
    _durationSubsScription?.cancel();
    _positionSubsScription?.cancel();
    _playerCompleteSubsScription?.cancel();
    _playerErrorSubsScription?.cancel();
    _playerStateSubsScription?.cancel();
    _seekCompleteSubscription?.cancel();

    Navigator.pop(context);
    _pause();
  }

  showTimePickerDialog()
  async {
    DatePicker.showTime12hPicker(context, showTitleActions: true, onChanged: (date) {
    print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
  }, onConfirm: (date) {
    dateTime=date;
    print('confirm $date');
  }, currentTime: DateTime.now());
    prefs = await SharedPreferences.getInstance();
    prefs.setString("link",items[indexQueue]["link"]);
    await AndroidAlarmManager.oneShotAt(
      dateTime,
      // Ensure we have a unique alarm ID.
      Random().nextInt(pow(2, 31)),
      callback,
      alarmClock: true,
      allowWhileIdle: true,
      rescheduleOnReboot: true,
      exact: true,
      wakeup: true,
    );
  }
  Future <void> initAlarm() async{
    // Register the UI isolate's SendPort to allow for communication from the
    // background isolate.
    IsolateNameServer.registerPortWithName(
      port.sendPort,
      isolateName,




    );
    prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(countKey)) {
      await prefs.setInt(countKey, 0);
    }
  }
  Future<void> _incrementCounter() async {
    print('Increment counter!');

    // Ensure we've loaded the updated count from the background isolate.
    await prefs.reload();

    setState(() {
      _counter++;
    });
  }
   static Future<void> callback() async {
    print('Alarm fired!');
    Fluttertoast.showToast(
        msg: "Alarm fired",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.orange,
        textColor: Colors.white,
        fontSize: 16.0);
    // Get the previous cached count and increment it.
    final prefs = await SharedPreferences.getInstance();
    urlAlarm=prefs.getString("link");
    print(prefs.getString('link'));
      int currentCount = prefs.getInt(countKey);
      await prefs.setInt(countKey, currentCount + 1);

    //FlutterRingtonePlayer.playAlarm(asAlarm:true);
      final result = await _audioPlayer.play("https://s3-ap-southeast-1.amazonaws.com/mazidombivli/20_05_17_06_01_08pm_Pasaydan.mp3",position:null,isLocal: false,respectSilence:false,stayAwake: true,volume:3.0);
    _audioPlayer.setPlaybackRate(playbackRate: 1.0);
      print(result);
      return result;
    // This will be null if we're running in the background.
    uiSendPort ??= IsolateNameServer.lookupPortByName(isolateName);
    uiSendPort?.send(null);
  }

}
