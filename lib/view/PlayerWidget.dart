import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:merge_map/merge_map.dart';
import 'package:shubham_karoti_kalynam/PlayerModel.dart';
import 'package:shubham_karoti_kalynam/colors/color.dart';
import 'package:shubham_karoti_kalynam/constants/StringResources.dart';
import 'package:shubham_karoti_kalynam/main.dart';
import 'package:shubham_karoti_kalynam/networking/ApiHandler.dart';
import 'package:shubham_karoti_kalynam/networking/ApiKeys.dart';
import 'package:shubham_karoti_kalynam/networking/EndApi.dart';
import 'package:shubham_karoti_kalynam/utils/PlayListSelector.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class PlayerWidget extends StatefulWidget {
  final title;
  final id;

  PlayerWidget(this.title, this.id);

  _PlayerWidgetState createState() => _PlayerWidgetState(id, title);
}

class _PlayerWidgetState extends State<PlayerWidget> {
  final id;
  final title;
  double descriptionSize = 14;
  Future<List<dynamic>> list;

  _PlayerWidgetState(this.id, this.title);

  final FirebaseMessaging _fcm = FirebaseMessaging();
  StreamSubscription iosSubscription;

  @override
  void initState() {
    super.initState();
    String catId = id.toString();
    list = ApiHandler.requestApi(catId + EndApi.ALL_DESCRIPTION);
    initNotification();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Container(
            color: color.colorConvert("#ECEFF1"),
            child: Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Container(
                    child: FutureBuilder<List<dynamic>>(
                        future: list,
                        builder: (context, snapshot) {
                          if (snapshot.hasError) {
                            return SafeArea(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    child: Center(
                                        child: Text(
                                  StringResources.NO_DATA_MESSAGE,
                                  style: TextStyle(
                                      fontSize: 17,
                                      color: Colors.orange,
                                      fontWeight: FontWeight.bold),
                                )))
                              ],
                            ));
                          }
                          return snapshot.hasData
                              ? ListWidget(items: snapshot.data, title: title)
                              : Center(
                                  child: CircularProgressIndicator(
                                    backgroundColor: Colors.orange,
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.white),
                                  ),
                                );
                        })))));
  }

  void initNotification() {
    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        print(data);
        // _saveDeviceToken();
      });

      _fcm.requestNotificationPermissions(IosNotificationSettings());
    } else {
      //_saveDeviceToken();
    }
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.cyan,
                child: Text(
                  'Ok',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
  }
}

enum PlayerState { stopped, playing, paused }

class ListWidget extends StatefulWidget {
  final List<dynamic> items;
  final title;

  ListWidget({Key key, this.items, this.title});

  List<String> urlList = [];
  final FirebaseMessaging _fcm = FirebaseMessaging();
  StreamSubscription iosSubscription;

  _ListWidgetState createState() => _ListWidgetState(items, title);
}

class _ListWidgetState extends State<ListWidget> {
  final List<dynamic> items;
  final title;
  double descriptionSize = 16;
  double titleSize = 18;

  _ListWidgetState(this.items, this.title);
  List <dynamic>listToRemove=[];
  var mapToRemove=new Map();
  List urlList = [];
  List playingList = [];
  List playListIconsList=[];
  AudioPlayer _audioPlayer;
  AudioPlayerState _audioPlayerState;
  Duration _duration;
  Duration _position;
  int indexQueue = 0;
  int ind = 0;
  String url;

  PlayerState _playerState = PlayerState.stopped;
  List<Color> _colors = [Colors.green, Colors.redAccent];
  List<double> _stops = [0.0, 0.7];
  StreamSubscription _durationSubsScription;
  StreamSubscription _positionSubsScription;
  StreamSubscription _playerCompleteSubsScription;
  StreamSubscription _playerErrorSubsScription;
  StreamSubscription _playerStateSubsScription;
  StreamSubscription _seekCompleteSubscription;
  ScrollController scrollController = new ScrollController();

  get _isPlaying => _playerState == PlayerState.playing;

  get _isPaused => _playerState == PlayerState.paused;

  get _durationText => _duration?.toString()?.split('.')?.first ?? '';

  get _positionText => _position?.toString()?.split('.')?.first ?? '';
  final _ScaffoldKey = new GlobalKey<ScaffoldState>();
  PlayerModel playerModel;
  PanelController _panelControler = new PanelController();

  List<dynamic> fList = [];
  List<dynamic> slist = [];

  Box<String> boxnewPlayList;
  var details = new Map();
  var idMap = new Map();
  var idMapTemp = new Map();
  bool addedtoPlaylist = false;
  String idsStr;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initAudioPlayer();
    indexQueue = 0;
    for (int i = 0; i < items.length; i++) {
      playingList.add(PlayerModel<String>("item $i"));
      urlList.add(items[i]['link']);
      playListIconsList.add(PlayListSelector<String>("item $i"));
      print(urlList.length);
      print(idsStr.toString());
      hidePanel();
    }
    boxnewPlayList = Hive.box(newPlayList);
    idsStr = boxnewPlayList.get("idMap");
    if (idsStr != null) {
      idsStr = boxnewPlayList.get("idMap");
      idMap = json.decode(idsStr);
    }
 /*   print("idmap_init: $idMap");
    print("string_init: {$idsStr}");*/
    // url = items[1]['link'];
  }

  var Position;

  @override
  void dispose() {
    _audioPlayer.dispose();
    _durationSubsScription?.cancel();
    _positionSubsScription?.cancel();
    _playerCompleteSubsScription?.cancel();
    _playerErrorSubsScription?.cancel();
    _playerStateSubsScription?.cancel();
    _seekCompleteSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (items.isNotEmpty) {
      return WillPopScope(
        onWillPop: () => _backPressed(),
        child: new Scaffold(
          key: _ScaffoldKey,
          appBar: AppBar(
            title: Text(title),
            backgroundColor: color.colorConvert(color.APP_COLOR),
            actions: <Widget>[
              Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(
                          Icons.zoom_in,
                          color: Colors.white,
                          size: 38,
                        ),
                        onPressed: () {
                          zoomIN();
                        })
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(10, 0, 12, 0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(
                          Icons.zoom_out,
                          color: Colors.white,
                          size: 38,
                        ),
                        onPressed: () => zoomOut())
                  ],
                ),
              )
            ],
          ),
          body: SlidingUpPanel(
            minHeight: 162,
            maxHeight: 160,
            controller: _panelControler,
            header: Container(
              height: 152,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: items[indexQueue]['title'] != "NULL"
                                ? Container(
                              margin: EdgeInsets.fromLTRB(8, 12, 0, 0),
                              child: Text(
                                items[indexQueue]['title'],
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.black45),
                                overflow: TextOverflow.ellipsis,
                                softWrap: true,
                              ),
                            )
                                : Text("\n"),
                          ),

                          Expanded(
                              flex: 1,
                              child: Container(
                                margin:
                                EdgeInsets.fromLTRB(
                                    20, 0, 0, 0),
                                child:
                                playListIconsList[indexQueue].isAdded?
                                IconButton(icon:Icon(Icons.playlist_add_check,size: 30,),
                                  onPressed:(){
                                    /*idMap.containsKey(items[index]["id"].toString())?
                                                                removeFromPlayList(index):
                                                                Icon(Icons.add);*/
                                    removeFromPlayList(indexQueue);
                                    setState(()=>playListIconsList[indexQueue].isAdded=false);
                                  },
                                )
                                    :idMap.isEmpty
                                    ? IconButton(icon: Icon(Icons.playlist_add,
                                  size: 30,
                                  color: Colors.orange,
                                ),
                                    onPressed: ()
                                    {
                                      details.clear();
                                      setState(() =>
                                      playListIconsList[indexQueue].isAdded=true);
                                      setState(() {
                                        Icon(Icons.playlist_add_check);
                                      });
                                      if (!idMap.containsKey(items[indexQueue]['id'].toString())) {
                                        details['id'] =items[indexQueue]['id'];
                                        details['title'] = items[indexQueue]['title'];
                                        details['description'] = items[indexQueue]['description'];
                                        details['link'] = items[indexQueue]['link'];

                                        idMapTemp[items[indexQueue]['id'].toString()] = items[indexQueue]['id'];
                                        addIdToMap(
                                            idMapTemp);
                                        addToPlayList(
                                            details);
                                      }
                                      else{
                                        idMap.containsKey(items[indexQueue]["id"].toString())?
                                        removeFromPlayList(indexQueue):
                                        Icon(Icons.add);
                                        setState(()=>playListIconsList[indexQueue].isAdded=false);
                                      }
                                    }):playListIconsList[indexQueue].isAdded?
                                IconButton(icon: Icon(Icons.playlist_add_check,
                                  size: 30,
                                  color: Colors.orange,
                                ),
                                    onPressed: ()
                                    {
                                    })
                                    : idMap.containsKey(items[indexQueue]['id'].toString()) ?
                                IconButton(
                                    icon: Icon(Icons.playlist_add_check,
                                      size: 30,
                                      color: Colors.black45,
                                    ),
                                    onPressed:
                                        () {
                                      details.clear();
                                      if (!idMap.containsKey(items[indexQueue]['id'].toString())) {
                                        details['id'] = items[indexQueue]['id'];
                                        details['title'] = items[indexQueue]['title'];
                                        details['description'] = items[indexQueue]['description'];
                                        details['link'] = items[indexQueue]['link'];
                                        idMapTemp[items[indexQueue]['id'].toString()] = items[indexQueue]['id'];
                                        setState(()
                                        {
                                          playListIconsList[indexQueue].isAdded=true;
                                          Icon(Icons
                                              .playlist_add_check);
                                        });
                                        addIdToMap(
                                            idMapTemp);
                                        addToPlayList(
                                            details);
                                      }

                                      else{
                                        idMap.containsKey(items[indexQueue]["id"].toString())?
                                        removeFromPlayList(indexQueue):
                                        Icon(Icons.add);
                                        setState(()=>playListIconsList[indexQueue].isAdded=false);
                                      }
                                    })
                                    : IconButton(
                                    icon: Icon(
                                      playListIconsList[indexQueue].isAdded?
                                      Icons.playlist_add_check
                                          :Icons.playlist_add,
                                      size: 30,
                                      color: Colors.orange,
                                    ),
                                    onPressed: () {
                                      details.clear();
                                      setState(() =>
                                      playListIconsList[indexQueue].isAdded=true);
                                      if (!idMap.containsKey(items[indexQueue]['id'].toString())) {
                                        details['id'] = items[indexQueue]['id'];
                                        details['title'] = items[indexQueue]['title'];
                                        details['description'] = items[indexQueue]['description'];
                                        details['link'] = items[indexQueue]['link'];
                                        idMapTemp[items[indexQueue]['id'].toString()] = items[indexQueue]['id'];
                                        addIdToMap(idMapTemp);
                                        addToPlayList(
                                            details);
                                      }

                                      else{
                                        idMap.containsKey(items[indexQueue]["id"].toString())?
                                        removeFromPlayList(indexQueue):
                                        Icon(Icons.add);
                                        setState(()=>playListIconsList[indexQueue].isAdded=false);
                                      }
                                    }),
                              )),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(10, 8, 25, 0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: _isPlaying
                                  ? Text(
                                _position != null
                                    ? '${_positionText ?? ''}'
                                    : _duration != null
                                    ? _durationText
                                    : '',
                                style: TextStyle(fontSize: 12.0),
                              )
                                  : Text("0:0:00"),
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 5, 10, 0),
                                height: 10,
                                child: Slider(
                                  activeColor:color.colorConvert(color.APP_COLOR),
                                  onChanged: (v) {
                                    Position = v * _duration.inMilliseconds;
                                    _audioPlayer.seek(
                                        Duration(milliseconds: Position.round()));
                                  },
                                  value: (_position != null &&
                                      _duration != null &&
                                      _position.inMilliseconds > 0 &&
                                      _position.inMilliseconds <
                                          _duration.inMilliseconds)
                                      ? _position.inMilliseconds /
                                      _duration.inMilliseconds
                                      : 0.0,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: _isPlaying
                                  ? Text(
                                _position != null
                                    ? '${_durationText ?? ''}'
                                    : _duration != null
                                    ? _durationText
                                    : '',
                                style: TextStyle(fontSize: 12.0),
                              )
                                  : Text("0:0:00"),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 20, 0),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 19),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.skip_previous,
                                    size: 50,
                                  ),
                                  color: Colors.black45,
                                  onPressed: () {
                                    skipPrevious();
                                  },
                                ),
                              ),
                            ),
                            Expanded(
                                child: Container(
                                  child: _isPlaying
                                      ? IconButton(
                                    key: Key('pause_button'),
                                    onPressed: () {
                                      playingList[indexQueue].isSelected =
                                      false;
                                      _pause();
                                    },
                                    iconSize: 45.0,
                                    icon: Icon(Icons.pause),
                                    color: Colors.black45,
                                  )
                                      : IconButton(
                                    iconSize: 45.0,
                                    icon: playingList[indexQueue].isSelected
                                        ? Icon(Icons.pause)
                                        : Icon(Icons.play_arrow),
                                    color: color.colorConvert(color.APP_COLOR),
                                    tooltip: "play_pause",
                                    key: Key('play_button'),
                                    onPressed: () {
                                      playingList[indexQueue].isSelected = true;
                                      //  url=items[index]['link'];
                                      setState(() => PlayerState.paused);
                                      setState(() {
                                        _playerState = PlayerState.paused;
                                        _duration = null;
                                        _position = null;
                                        _play();
                                      });
                                    },
                                  ),
                                )),
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 15),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.skip_next,
                                    color: Colors.black45,
                                    size: 50,
                                  ),
                                  onPressed: () {
                                    skipNext();
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //  ),
            panel: Center(),
            body: Container(
              color: Colors.black12,
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              width: MediaQuery.of(context).size.width - 50,
              child: Column(
                children: <Widget>[
                  new Expanded(
                    child: new ListView.builder(
                      itemCount: items.length,
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 248),
                      controller: scrollController,
                      itemBuilder: (context, index) {
                        return new InkWell(
                          onTap: () {
                            ind = index;
                            url = items[index]['link'];
                          },
                          child: Container(
                            padding: EdgeInsets.fromLTRB(3, 0, 3, 0),
                            width: MediaQuery.of(context).size.width,
                            child: Card(
                              elevation: 0,
                              color: color.colorConvert("#ffffff"),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.fromLTRB(1, 0, 2, 0),
                                    ),
                                    Expanded(
                                        child: Container(
                                            padding:
                                            EdgeInsets.fromLTRB(2, 13, 2, 26),
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                              MainAxisAlignment.start,
                                              children: <Widget>[
                                                Row(
                                                  mainAxisSize: MainAxisSize.max,
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceEvenly,
                                                  children: <Widget>[
                                                    /*Padding(
                                                    padding: EdgeInsets.fromLTRB(
                                                        20, 0, 30, 0),
                                                    child:*/
                                                    Expanded(
                                                      flex: 4,
                                                      child: items[index]
                                                      ['title'] !=
                                                          "NULL"
                                                          ? Container(
                                                        margin: EdgeInsets
                                                            .fromLTRB(20, 4,
                                                            0, 0),
                                                        child: Text(
                                                            items[index][
                                                            ApiKeys
                                                                .TITLE],
                                                            overflow:
                                                            TextOverflow
                                                                .clip,
                                                            softWrap: true,
                                                            style: GoogleFonts.roboto(
                                                                fontWeight:
                                                                FontWeight
                                                                    .bold,
                                                                color: Colors
                                                                    .black54,
                                                                fontSize:
                                                                titleSize)),
                                                      )
                                                          : Text(""),
                                                    ),

                                                    Expanded(
                                                        flex: 1,
                                                        child: Container(
                                                          margin:
                                                          EdgeInsets.fromLTRB(
                                                              22, 0, 0, 0),
                                                          child:
                                                          playListIconsList[index].isAdded?
                                                          IconButton(icon:Icon(Icons.playlist_add_check,size: 30,color:Colors.black54,),
                                                            onPressed:(){
                                                              idMap.containsKey(items[index]["id"].toString())?
                                                              removeFromPlayList(index):
                                                              Icon(Icons.add);
                                                              removeFromPlayList(index);
                                                              setState(()=>playListIconsList[index].isAdded=false);
                                                            },
                                                          )
                                                              :idMap.isEmpty
                                                              ? IconButton(icon: Icon(Icons.playlist_add,
                                                            size: 30,
                                                            color: Colors.orange,
                                                          ),
                                                              onPressed: ()
                                                              {
                                                                details.clear();
                                                                setState(() =>
                                                                playListIconsList[index].isAdded=true);
                                                                setState(() {
                                                                  Icon(Icons.playlist_add_check);
                                                                });
                                                                if (!idMap.containsKey(items[index]['id'].toString())) {
                                                                  details['id'] =items[index]['id'];
                                                                  details['title'] = items[index]['title'];
                                                                  details['description'] = items[index]['description'];
                                                                  details['link'] = items[index]['link'];

                                                                  idMapTemp[items[index]['id'].toString()] = items[index]['id'];
                                                                  addIdToMap(
                                                                      idMapTemp);
                                                                  addToPlayList(
                                                                      details);
                                                                }
                                                                else{
                                                                  idMap.containsKey(items[index]["id"].toString())?
                                                                  removeFromPlayList(index):
                                                                  Icon(Icons.add);
                                                                  setState(()=>playListIconsList[index].isAdded=false);
                                                                }
                                                              }):playListIconsList[index].isAdded?
                                                          IconButton(icon: Icon(Icons.playlist_add_check,
                                                            size: 30,
                                                            color: Colors.orange,
                                                          ),
                                                              onPressed: ()
                                                              {
                                                              })
                                                              : idMap.containsKey(items[index]['id'].toString()) ?
                                                          IconButton(
                                                              icon: Icon(Icons.playlist_add_check,
                                                                size: 30,
                                                                color: Colors.black45,
                                                              ),
                                                              onPressed:
                                                                  () {
                                                                details.clear();
                                                                if (!idMap.containsKey(items[index]['id'].toString())) {
                                                                  details['id'] = items[index]['id'];
                                                                  details['title'] = items[index]['title'];
                                                                  details['description'] = items[index]['description'];
                                                                  details['link'] = items[index]['link'];
                                                                  idMapTemp[items[index]['id'].toString()] = items[index]['id'];
                                                                  setState(()
                                                                  {
                                                                    playListIconsList[index].isAdded=true;
                                                                    Icon(Icons
                                                                        .playlist_add_check);
                                                                  });
                                                                  addIdToMap(
                                                                      idMapTemp);
                                                                  addToPlayList(
                                                                      details);
                                                                }

                                                                else{
                                                                  idMap.containsKey(items[index]["id"].toString())?
                                                                  removeFromPlayList(index):
                                                                  Icon(Icons.add);
                                                                  setState(()=>playListIconsList[index].isAdded=false);
                                                                }
                                                              })
                                                              : IconButton(
                                                              icon: Icon(
                                                                playListIconsList[index].isAdded?
                                                                Icons.playlist_add_check
                                                                    :Icons.playlist_add,
                                                                size: 30,
                                                                color: Colors.orange,
                                                              ),
                                                              onPressed: () {
                                                                details.clear();
                                                                setState(() =>
                                                                playListIconsList[index].isAdded=true);
                                                                if (!idMap.containsKey(items[index]['id'].toString())) {
                                                                  details['id'] = items[index]['id'];
                                                                  details['title'] = items[index]['title'];
                                                                  details['description'] = items[index]['description'];
                                                                  details['link'] = items[index]['link'];
                                                                  idMapTemp[items[index]['id'].toString()] = items[index]['id'];
                                                                  addIdToMap(idMapTemp);
                                                                  addToPlayList(
                                                                      details);
                                                                }

                                                                else{
                                                                  idMap.containsKey(items[index]["id"].toString())?
                                                                  removeFromPlayList(index):
                                                                  Icon(Icons.add);
                                                                  setState(()=>playListIconsList[index].isAdded=false);
                                                                }
                                                              }),
                                                        )),
                                                    Expanded(
                                                      flex: 2,
                                                      child: Container(
                                                          margin:
                                                          EdgeInsets.fromLTRB(
                                                              10, 0, 10, 0),
                                                          child: items[index]
                                                          ['link'] ==
                                                              "NULL"
                                                              ? Text("")
                                                              : playingList[index]
                                                              .isSelected
                                                              ? IconButton(
                                                            onPressed:
                                                                () {
                                                              _pause();
                                                              setState(
                                                                      () {
                                                                    playingList[index].isSelected =
                                                                    false;
                                                                  });
                                                            },
                                                            iconSize:
                                                            35.0,
                                                            icon: Icon(Icons
                                                                .pause),
                                                            color: Colors
                                                                .black45,
                                                          )
                                                              : IconButton(
                                                            onPressed:
                                                                () {
                                                              setState(
                                                                      () {
                                                                    ind =
                                                                        indexQueue;
                                                                    playingList[ind].isSelected =
                                                                    false;
                                                                    indexQueue =
                                                                        index;
                                                                    url = items[indexQueue]
                                                                    [
                                                                    'link'];
                                                                    playingList[index].isSelected =
                                                                    true;
                                                                  });

                                                              _play();
                                                            },
                                                            iconSize:
                                                            35.0,
                                                            icon: Icon(Icons
                                                                .play_arrow),
                                                            color: color
                                                                .colorConvert(
                                                                color.APP_COLOR),
                                                          )),
                                                    )
                                                  ],
                                                ),
                                                items[index]['description'] !=
                                                    "NULL"
                                                    ? Column(
                                                  children: <Widget>[
                                                    Padding(
                                                        padding: EdgeInsets
                                                            .fromLTRB(20, 0,
                                                            10, 0),
                                                        child: new Text(
                                                          items[index][
                                                          'description'],
                                                          style: GoogleFonts.roboto(
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              fontSize:
                                                              descriptionSize,
                                                              color: Colors
                                                                  .black45),
                                                        )),
                                                  ],
                                                )
                                                    : Row(
                                                    mainAxisSize:
                                                    MainAxisSize.min),
                                              ],
                                            )))
                                  ]),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }
    if (items.isEmpty) {
      return Scaffold(
        appBar:AppBar(title: Text(StringResources.PLAY_LIST),backgroundColor:Colors.orange,),
        body:
        SafeArea(
            child:Column(
              mainAxisAlignment:MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    child:Center(
                        child:Text(StringResources.NO_DATA_MESSAGE,
                          style:TextStyle(fontSize:17,color:Colors.orange,fontWeight:FontWeight.bold),)
                    )
                )
              ],
            )
        ),
      );
    }
  }
  void skipNext() {
    if (indexQueue < items.length) {
      setState(() => _playerState = PlayerState.stopped);
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = null;
        _position = null;
        playingList[indexQueue].isSelected = false;
        indexQueue < items.length - 1
            ? indexQueue = indexQueue + 1
            : Scaffold.of(context)
                .showSnackBar(SnackBar(content: Text("Queue Finished")));
        url = items[indexQueue]['link'];
        //playingList.add(PlayerModel<String>("item $indexQueue"));
        playingList[indexQueue].isSelected = true;
        _play();
      });
    }
  }

  void zoomIN() async {
    setState(() {
      descriptionSize < 20 ? descriptionSize += 1 : descriptionSize = 20;
      titleSize < 20 ? titleSize += 3 : titleSize = 20;
    });
  }

  void zoomOut() async {
    setState(() {
      descriptionSize >16
          ? descriptionSize -= 1
          :descriptionSize=16;
      titleSize > 18 ? titleSize -= 2 :titleSize=18;
      print(descriptionSize);
    });
  }

  finishZoom() {
    Fluttertoast.showToast(
        msg: StringResources.DEFAULT_SIZE,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.orange,
        textColor: Colors.white,
        fontSize: 16.0);
    descriptionSize = 15;
    titleSize = 18;
  }

  void skipPrevious() {
    if (indexQueue != 0) {
      color:
      Colors.black45;
      setState(() => _playerState = PlayerState.stopped);
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = null;
        _position = null;
        playingList[indexQueue].isSelected = false;
        indexQueue = indexQueue - 1;
        url = items[indexQueue]['link'];
        playingList[indexQueue].isSelected = true;
        _play();
      });
    }
    if (indexQueue == 0) {
      color:
      Colors.black12;
      Scaffold.of(context).showSnackBar(SnackBar(content: Text("Unavailable")));
    }
  }

  void _initAudioPlayer() {
    _audioPlayer = AudioPlayer();
    int index = 0;
    _durationSubsScription = _audioPlayer.onDurationChanged.listen((duration) {
      setState(() => _duration = duration);

      // TODO implemented for iOS, waiting for android impl
      if (Theme.of(context).platform == TargetPlatform.iOS) {
        // (Optional) listen for notification updates in the background
        _audioPlayer.startHeadlessService();
      }
    });

    _positionSubsScription =
        _audioPlayer.onAudioPositionChanged.listen((p) => setState(() {
              _position = p;
            }));
    _seekCompleteSubscription = _audioPlayer.onSeekComplete.listen((event) {
      //_onSeekComplete();
    });
    _playerCompleteSubsScription =
        _audioPlayer.onPlayerCompletion.listen((event) {
      setState(() {
        _position = _duration;
        _onComplete();
        playingList[indexQueue].isSelected = false;
        url = urlList[indexQueue + 1];
        indexQueue = indexQueue + 1;
        playingList[indexQueue].isSelected = true;
        _play();
      });
    });

    _playerErrorSubsScription = _audioPlayer.onPlayerError.listen((msg) {
      print('audioPlayer error : $msg');
      setState(() {
        _playerState = PlayerState.stopped;
        _duration = Duration(seconds: 0);
        _position = Duration(seconds: 0);
      });
    });

    _audioPlayer.onPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() {
        _audioPlayerState = state;
      });
    });

    _audioPlayer.onNotificationPlayerStateChanged.listen((state) {
      if (!mounted) return;
      setState(() => _audioPlayerState = state);
    });
  }

  Future<int> _play() async {
    _panelControler.show();
    _duration = null;
    _position = null;
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(
          "Please Wait Streaming....",
          style: GoogleFonts.roboto(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 12),
        ),
        backgroundColor: Colors.white10,
        duration: Duration(seconds: 4),
      ),
    );
    final playPosition = (_position != null &&
            _duration != null &&
            _position.inMilliseconds > 0 &&
            _position.inMilliseconds < _duration.inMilliseconds)
        ? _position
        : null;
    final result = await _audioPlayer.play(url, position: playPosition);
    if (result == 1) setState(() => _playerState = PlayerState.playing);

    _audioPlayer.setPlaybackRate(playbackRate: 1.0);
    return result;
  }

  Future<int> _pause() async {
    final result = await _audioPlayer.pause();
    if (result == 1) setState(() => _playerState = PlayerState.paused);
    return result;
  }

  void _onComplete() {
    setState(() => _playerState = PlayerState.playing);
  }

  void hidePanel() {
    Future.delayed(Duration(microseconds: 1), () => _panelControler.hide());
  }

  _backPressed() {
    setState(() {
      setState(() => _playerState = PlayerState.stopped);
    });
    _audioPlayer.dispose();
    _durationSubsScription?.cancel();
    _positionSubsScription?.cancel();
    _playerCompleteSubsScription?.cancel();
    _playerErrorSubsScription?.cancel();
    _playerStateSubsScription?.cancel();
    _seekCompleteSubscription?.cancel();

    Navigator.pop(context);
    _pause();
  }

  addToPlayList(Map map) {
    fList.clear();
    slist.clear();
    fList.add(map);
    String str = json.encode(fList);
    String previousString=null;
    boxnewPlayList.get("playList") == null
        ? boxnewPlayList.put("playList", str)
        : previousString = boxnewPlayList.get("playList");

    if (previousString != null) {
      slist = json.decode(previousString);
      var newList = new List.from(slist)..addAll(fList);
      str = json.encode(newList);
      boxnewPlayList.put("playList", str);
      map.clear();
      details.clear();
    }
    //TODO PLAYLIST

     debugPrint("mapfromlist: $str", wrapWidth:4096);
    //debugPrint(boxnewPlayList.get("playList"), wrapWidth:1024);

    Fluttertoast.showToast(
        msg: StringResources.ADDED_TO_PLAY_LIST.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.orange,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  void addIdToMap(Map idMapTemp) {
    print(idMapTemp);
    if (idsStr != null) {
      Map merged = mergeMap([idMap, idMapTemp]);
      print(merged);
      idsStr = json.encode(merged);
      boxnewPlayList.put("idMap", idsStr);
    }
    if (idsStr == null) {
      idsStr = json.encode(idMapTemp);
      boxnewPlayList.put("idMap", idsStr);
      print(boxnewPlayList.get("idMap"));
      /*idMapTemp.clear();
      idsStr=null;*/
    }
  }

  removeFromPlayList(item) {
    String strGetList=boxnewPlayList.get("playList");
    listToRemove=json.decode(strGetList);

print(idsStr);
    String idStr=boxnewPlayList.get("idMap");
    idMap=json.decode(idStr);
    idMap.remove(items[item]["id"].toString());
    idStr=json.encode(idMap);

    listToRemove.removeAt(item);
    String str=json.encode(listToRemove);
    boxnewPlayList.put("idMap", idStr);
    boxnewPlayList.put("playList", str);
    print(items[item]["id"].toString());
    print(idsStr);

    Fluttertoast.showToast(
        msg: StringResources.REMOVED_FROM_PLAY_LIST.toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.orange,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}
