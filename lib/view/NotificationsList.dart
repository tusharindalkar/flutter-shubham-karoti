
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hive/hive.dart';
import 'package:shubham_karoti_kalynam/colors/color.dart';

class NotificationsList extends StatefulWidget
{
  _NotificationsList createState()=> _NotificationsList();
}

class _NotificationsList extends State<NotificationsList>
{
  List<dynamic>notificationList=[];
  Box<String> notificationListBox;
  String strResponse;
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    notificationListBox = Hive.box("notifications");
   strResponse=notificationListBox.get("notifications");
    if (strResponse!=null) {
      notificationList=json.decode(strResponse);
    }

}
  @override
  Widget build(BuildContext context)
  {
    // TODO: implement build
   return Scaffold(
     appBar:AppBar(
       title: Text("Notifications"),
       backgroundColor:Colors.orange,
     ),
     body:
         Container(
           margin:EdgeInsets.fromLTRB(0, 4, 0, 0),
             color: color.colorConvert("#ECEFF1"),
             child:
             notificationList.isNotEmpty?
             ListView.builder(
              itemCount:notificationList.length,
              itemBuilder:(context,index){
                return GestureDetector(
                  child:Container(
                    padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
                    height: 70,
                    child: Card(
                        color: color.colorConvert("#ffffff"),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child:Row(
                          mainAxisSize:MainAxisSize.min,
                          mainAxisAlignment:MainAxisAlignment.spaceBetween,
                          crossAxisAlignment:CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin:EdgeInsets.fromLTRB(25, 4, 0, 0),
                            child:Column(
                              children: <Widget>[
                                Expanded(
                              //  flex:2,
                                  child:Text(notificationList[index]["title"]
                                    ,style:TextStyle(color:Colors.black54,fontSize:16,fontWeight:FontWeight.bold),),
                                ),
                                Expanded(
                                //  flex:2,
                                  child:Text(notificationList[index]["message"],
                                  style:TextStyle(fontSize: 14,fontWeight:
                                  FontWeight.bold,color:Colors.black38),),
                                )
                              ],
                            ),
                            )
                          ],
                        )
                    ),
                  ),
                );
              }):
                 Center(
                   child:Text("You don't have any notificaions"),
                 )
         )
   );
    }
}