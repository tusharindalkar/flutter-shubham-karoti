import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:shubham_karoti_kalynam/colors/color.dart';
import 'package:shubham_karoti_kalynam/constants/StringResources.dart';
import 'package:shubham_karoti_kalynam/networking/ApiHandler.dart';
import 'package:shubham_karoti_kalynam/networking/ApiKeys.dart';
import 'package:shubham_karoti_kalynam/networking/ApiProvider.dart';
import 'package:shubham_karoti_kalynam/networking/EndApi.dart';
import 'package:shubham_karoti_kalynam/view/NotificationsList.dart';
import 'package:shubham_karoti_kalynam/view/PlayListWidget.dart';
import 'package:shubham_karoti_kalynam/view/PlayerWidget.dart';
import 'package:shubham_karoti_kalynam/view/WebView.dart';
import 'package:flutter_share_me/flutter_share_me.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:io';
import 'package:shubham_karoti_kalynam/_AlarmHomePage.dart';
import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:http/http.dart'as http;
import 'package:package_info/package_info.dart';
class MyHomePage extends StatefulWidget {

  final id;
  final title;
  bool flag;
  MyHomePage(this.id,this.title,this.flag);
    _MyHomePageState createState() => _MyHomePageState(id,title,flag);
}

class _MyHomePageState extends State<MyHomePage>
{
   Future<List<dynamic>> categories;
int id;
final title;
bool flag;
   String oldVersion;
   String notificationTitle;
   String updateMessage;
  _MyHomePageState(this.id, this.title,this.flag);
   final FirebaseMessaging _fcm = FirebaseMessaging();
   StreamSubscription iosSubscription;
   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
   Box<String> notificationListBox;
  List<dynamic>notificationList=[];
  List<dynamic>tempList=[];
  var notificationMap=new Map();
  String notificationStr;
   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    categories =
        ApiHandler.requestApi(id.toString()+EndApi.ALL_CATEGORIES);
    notificationListBox = Hive.box("notifications");
    notificationStr=notificationListBox.get("notifications");
    if (notificationStr!=null) {
      print("NOTI $notificationStr");
    }
    initNotification();
    try {
      if (Platform.isAndroid) {
        getVersion(context);
      }
    } catch (e) {
      print(e);
    }
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if(id==0)
      {
        return Scaffold(
          key: _scaffoldKey,
        body:
          NestedScrollView(
              headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverOverlapAbsorber(
                      handle:
                      NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                      child: SliverAppBar(
                        leading: IconButton(icon: Icon(Icons.menu,size: 28,color:Colors.white,),
                          onPressed: () => _scaffoldKey.currentState.openDrawer(),),
                        actions: <Widget>[
                          Container(
                            margin:EdgeInsets.fromLTRB(0, 0, 10, 0),
                            child:IconButton(icon: Icon(Icons.notifications,
                              color:Colors.white,size: 28,),onPressed: ()=>
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>AlarmHomePage()))
                              ,)

                          ),
                        ],
                        title:Text(title),
                        expandedHeight: 150.0,
                        floating: false,
                        pinned: true,
                        backgroundColor:Colors.orange,
                        flexibleSpace: FlexibleSpaceBar(
                            centerTitle: true,
                            background: Container(
                              child: Image.asset('assets/images/navban.png',width:400,fit:BoxFit.fill,),
                            )),
                        forceElevated: innerBoxIsScrolled,
                      )),
                ];
              },
              body:
              Center(
                child: Container(
                  margin:EdgeInsets.fromLTRB(0, 60, 0, 0),
                  color: color.colorConvert("#ECEFF1"),
                  child: Center(
                    child: FutureBuilder<List<dynamic>>(
                      future: categories,
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          return SafeArea(
                              child:Column(
                                mainAxisAlignment:MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                      child:Center(
                                          child:Text(StringResources.NO_DATA_MESSAGE,
                                            style:TextStyle(fontSize:17,color:Colors.orange,fontWeight:FontWeight.bold),)
                                      )
                                  )
                                ],
                              )
                          );
                        }
                        return snapshot.hasData
                            ? CategoriesBoxList(items: snapshot.data,id:id)
                            : Center(
                          child: CircularProgressIndicator(backgroundColor:Colors.orange,
                            valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              )
            /* SafeArea(
              top: false,
              bottom: false,
              child:

                ),*/
          ),

          drawer:Drawer(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      DrawerHeader(

                        decoration: BoxDecoration(
                          image: DecorationImage(image: AssetImage('assets/images/last.png'),fit:BoxFit.fill),
                          /*  color:color.colorConvert(color.APP_COLOR)*/),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(0 , 2, 1, 0),
                              /*child: Image.asset(
                                    "assets/images/Iconnav.png",height: 95,
                                    width: 95,
                                  ),*/
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(16, 30, 1, 1),
                              child: new Text(
                                StringResources.APP_NAME,
                                style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                              ),
                            )
                          ],
                        ),
                      ),
                      ListTile(
                        title: Text(
                          StringResources.HOME,
                          style: GoogleFonts.roboto(
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                        leading: Icon(Icons.home,color: Colors.orangeAccent,size: 27,),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                      ListTile(
                        leading: Icon(Icons.bookmark,color: Colors.orangeAccent,size: 27,),
                        title: Text(
                          StringResources.PLAY_LIST,
                          style: GoogleFonts.roboto(
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>PlayListWidget()));
                        },
                      ),
                      ListTile(
                        title: Text(
                          StringResources.CONTACT_US,
                          style: GoogleFonts.roboto(
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                              fontSize: 14),
                        ),
                        onTap: (){
                          _launchURL('contact.us@arraypointer.com',StringResources.APP_NAME, 'Hello,');
                                },
                        leading: Icon(Icons.perm_contact_calendar,color: Colors.orangeAccent,size: 25),
                      ),
                      ListTile(
                        title: Text(StringResources.TERMS_OF_USE,style: TextStyle(fontSize: 14,
                            fontWeight: FontWeight.bold,color: Colors.black54),),
                        leading: Icon(Icons.description,color: Colors.orangeAccent,size: 25,),
                        onTap: (){
                          String title="";
                          String url=StringResources.TERMS_LINK;
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>MyWebView(title: title,selectedUrl: url,)));
                        },
                      ),
                      ListTile(
                        title:Text(StringResources.SHARE,style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold,
                            color: Colors.black54),) ,
                        leading: Icon(Icons.share,color: Colors.orangeAccent,size: 25,),
                        onTap: () async {
                          var response = await FlutterShareMe().shareToSystem(msg: StringResources.APP_LINK+" "+StringResources.SHARE_TEXT);
                          if (response == 'success') {
                            print('navigate success');
                          }
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                    margin:EdgeInsets.fromLTRB(20, 0, 4, 30),
                    child:Column(
                      children: <Widget>[
                        Divider(),
                        Row(
                      children: <Widget>[
                            Container(
                              margin:EdgeInsets.fromLTRB(15, 0, 0, 0),
                              child:Text(StringResources.APP_VERSION,style:TextStyle(fontSize:11,
                                  fontWeight:FontWeight.bold,color:Colors.black45),),
                            )
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              margin:EdgeInsets.fromLTRB(15, 5, 0, 0),
                              child:  Text(StringResources.CRAFTED_TEXT,style: TextStyle(fontSize:11,
                                  fontWeight:FontWeight.bold,color:Colors.black45),overflow:TextOverflow.clip,
                                softWrap: true,),
                            )
                          ],
                        )

                      ],
                    )
                )
              ],
            ),
          ),
        );
      }
    if(id!=0)
      {
        return Scaffold(
        appBar: AppBar(
          title: Text(
            title,
            style: GoogleFonts.roboto(
                fontSize: 18,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
          backgroundColor:color.colorConvert(color.APP_COLOR),
          leading: Builder(builder: (BuildContext context){
            return id==0?IconButton(
              icon: const Icon(Icons.menu,size:50),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            ):IconButton(
              icon: const Icon(Icons.arrow_back,size: 24,),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          }),
        ),
          body:
          Center(
            child: Container(
              margin:EdgeInsets.fromLTRB(0,4, 0, 0),
              color: color.colorConvert("#ECEFF1"),
              child: Center(
                child: FutureBuilder<List<dynamic>>(
                  future: categories,
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      Scaffold.of(context).showSnackBar(
                          SnackBar(content: Text(StringResources.APIERROR)));
                    }
                    return snapshot.hasData
                        ? CategoriesBoxList(items: snapshot.data,id:id)
                        : Center(
                      child: CircularProgressIndicator(backgroundColor:Colors.orange,
                        valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                    );
                  },
                ),
              ),
            ),
          )
        );
      }
  }


   _launchURL(String toMailId, String subject, String body) async {
     var url = 'mailto:$toMailId?subject=$subject&body=$body';
     if (await canLaunch(url)) {
       await launch(url);
     } else {
       throw 'Could not launch $url';
     }
   }

  void initNotification()
  {
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        updateMessage=message['notification']["body"];
        notificationTitle=message['notification']['title'];
        _showDialog();
        notificationMap["title"]=notificationTitle;
        notificationMap["message"]=updateMessage;
        saveNotification(notificationMap);

      },
      onLaunch: (Map<String, dynamic> message) async {
        _launchPlaystore(StringResources.APP_LINK);
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        _launchPlaystore(StringResources.APP_LINK);
        // TODO optional
      },
    );
  }

   _launchPlaystore(String url) async {
     if (await canLaunch(url)) {
       await launch(url);
     } else {
       throw 'Could not launch $url';
     }
   }

  Future<void> getVersion(BuildContext context) async {
    String projectVersion;
// Platform messages may fail, so we use a try/catch PlatformException.
    try {

      var client=new http.Client();
      final response=await client.get(ApiProvider.APIUPDATE);
      if(response.statusCode==200)
        {
          //print(response.body);
          Map<String,dynamic>map=json.decode(response.body);
          Map mapResult=map[ApiKeys.RESULT];
          print(mapResult['name']);
          oldVersion=mapResult['name'];
          updateMessage=mapResult['message'];
        }
      String version;
      String buildNumber;
      PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
        String appName = packageInfo.appName;
        String packageName = packageInfo.packageName;
         version = packageInfo.version;
         buildNumber = packageInfo.buildNumber;
        if (version!=oldVersion) {
        //   _showDialog();
         }
        print(version);
      });
         } on PlatformException {
      projectVersion = 'Failed to get project version.';
    }
  }
   void _showDialog() {
     // flutter defined function
     showDialog(
       context: context,
       barrierDismissible:false,
       builder: (BuildContext context) {
         // return object of type Dialog
         return AlertDialog(
           title: new Text(notificationTitle,style:TextStyle(fontSize:16,color:Colors.black87,fontWeight:FontWeight.bold),),
           content: new Text(updateMessage,style:TextStyle(fontSize:14,color:Colors.black54,fontWeight:FontWeight.bold)),
           actions: <Widget>[
             new FlatButton( child: new Text("Remind me Later",style:TextStyle(fontSize: 15,color:Colors.black45,fontWeight:FontWeight.bold)),
                 onPressed: ()=>Navigator.of(context).pop()
             ),
             new FlatButton(
               child: new Text("Ok",style:TextStyle(fontSize:15,color:Colors.cyan,fontWeight:FontWeight.bold),),
               onPressed: () {
                 Navigator.of(context).pop();
                 _launchPlaystore(StringResources.APP_LINK);
               },
             ),
           ],
         );
       },
     );
   }

  void saveNotification(Map notificationMap)
  {
    tempList.clear();
    notificationList.clear();
    notificationList.add(notificationMap);
    notificationStr=json.encode(notificationList);
    if (notificationStr!=null) {
      tempList=json.decode(notificationStr);
      var newList=new List.from(tempList)..addAll(notificationList);
      String str=json.encode(newList);
      notificationListBox.put("notifications", str);

    }
    else{
        notificationListBox.put("notifications", notificationStr);

    }
  }

}

class CategoriesBoxList extends StatefulWidget {
  final List<dynamic> items;
  final id;
  CategoriesBoxList({Key key, this.items,item,this.id});

  _CategoriesBoxList createState() => _CategoriesBoxList(items,id);
}

class _CategoriesBoxList extends State<CategoriesBoxList> {
  final List<dynamic> items;
  final id;
  _CategoriesBoxList(this.items,this.id);

  var count;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: items.length,
      itemBuilder: (context, index) {
        return GestureDetector(
            onTap: () {
              print(items[index]['title']);
              /*Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage(title: "title",)),
            );*/
              Navigator.push(context, new MaterialPageRoute(builder: (context) {
                int isParent;
                isParent=items[index]['isParent'];
              if (isParent==0) {
                return new PlayerWidget(
                    items[index][ApiKeys.TITLE],items[index]['id']);
              }
              else{
                return new MyHomePage(
                    items[index]['id'],items[index][ApiKeys.TITLE],true);
              }
              }));
            },
            child: Container(
                padding: EdgeInsets.fromLTRB(4,0, 5, 0),
                height: 90,
                child: Card(
                  elevation: 0,
                  color: color.colorConvert("#ffffff"),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(13)),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 2, 2, 2),
                          child: ClipOval(
                            child:
                            CachedNetworkImage(
                              height:55,
                              width:55,
                              fit:BoxFit.cover,
                              imageUrl:items[index]['image'],
                              placeholder: (context, url) => CircularProgressIndicator(backgroundColor:Colors.orangeAccent,
                                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                              ),
                              errorWidget: (context, url, error) => Icon(Icons.error),
                            )
                          ),
                        ),
                        Expanded(
                            child: Container(
                                padding: EdgeInsets.fromLTRB(14, 2, 2, 2),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(items[index][ApiKeys.TITLE],
                                        style: GoogleFonts.roboto(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black54,
                                            fontSize: 15))
                                  ],
                                )))
                      ]),
                )));
      },
    );
  }
}
