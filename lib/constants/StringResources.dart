import 'package:flutter/cupertino.dart';

class StringResources
{
  static String APP_NAME="शुभंकरोति कल्याणम्";
  static String HOME="मुख्यपृष्ठ";
  static String PLAY_LIST="प्लेलिस्ट";
  static String ADDED_TO_PLAY_LIST="प्लेलिस्टमध्ये जोडले";
  static String REMOVED_FROM_PLAY_LIST="प्लेलिस्टमधून काढले";

  static String CONTACT_US="आमच्याशी संपर्क साधा";
  static String TERMS_OF_USE="वापरण्याच्या अटी";
  static String SHARE="आमंत्रित कर";
  static String APIERROR="Unable to fetch data from the  API";
  static String APP_LINK="https://play.google.com/store/apps/details?id=shubhamkarotikalyanam.arraypointer.com.shubham_karoti_kalynam";
  static String TERMS_LINK="http://shubhamkaroti.arraypointer.com/#/terms";
  static String SHARE_TEXT="Hey there I am using शुभंकरोति कल्याणम् app to install click here";
  static String APP_VERSION="App Version -V 2.0.0";
  static String CRAFTED_TEXT="Crafted with 🙏 devotion by [ ArrayPoiner ]*";
  static String PACKAGE_NAME="shubhamkarotikalyanam.arraypointer.com.shubham_karoti_kalynam";
  static String PLEASE_WAIT="कृपया थांबा";
  static String DEFAULT_SIZE="मुळ आकार";
  static String ADDED_TO_PLAYlIST="प्लेलिस्टमध्ये जोडले";
  static String NO_DATA_MESSAGE="Sorry no data available \n  Please try again later";
  static String EMPTY_PLAYlIST="रिक्त प्लेलिस्ट कृपया काहीतरी जोडा";
  static String NOTOFICATION_Title="Update Available";
  static String NOTIFICATION_DESCRIPTION="Please update to get more features";

}