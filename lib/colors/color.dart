
import 'package:flutter/material.dart';

class color {

  static String cardColor = "#000000";
  static String appBackgroundColor = "#000000";
  static String APP_COLOR ="#FFA500";


  static Color colorConvert(String color) {
    color = color.replaceAll("#", "");
    if (color.length == 6) {
      return Color(int.parse("0xFF" + color));
    } else if (color.length == 8) {
      return Color(int.parse("0x" + color));
    }
  }
}